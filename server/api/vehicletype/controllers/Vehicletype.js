'use strict';

/**
 * Vehicletype.js controller
 *
 * @description: A set of functions called "actions" for managing `Vehicletype`.
 */

module.exports = {

  /**
   * Retrieve vehicletype records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.vehicletype.search(ctx.query);
    } else {
      return strapi.services.vehicletype.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a vehicletype record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.vehicletype.fetch(ctx.params);
  },

  /**
   * Count vehicletype records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.vehicletype.count(ctx.query);
  },

  /**
   * Create a/an vehicletype record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.vehicletype.add(ctx.request.body);
  },

  /**
   * Update a/an vehicletype record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.vehicletype.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an vehicletype record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.vehicletype.remove(ctx.params);
  }
};
