'use strict';

/**
 * Vehiclesubtype.js controller
 *
 * @description: A set of functions called "actions" for managing `Vehiclesubtype`.
 */

module.exports = {

  /**
   * Retrieve vehiclesubtype records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.vehiclesubtype.search(ctx.query);
    } else {
      return strapi.services.vehiclesubtype.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a vehiclesubtype record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.vehiclesubtype.fetch(ctx.params);
  },

  /**
   * Count vehiclesubtype records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.vehiclesubtype.count(ctx.query);
  },

  /**
   * Create a/an vehiclesubtype record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.vehiclesubtype.add(ctx.request.body);
  },

  /**
   * Update a/an vehiclesubtype record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.vehiclesubtype.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an vehiclesubtype record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.vehiclesubtype.remove(ctx.params);
  }
};
