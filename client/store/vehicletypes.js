export const state = () => ({
  list: []
})

export const mutations = {
  add(state, vehicletype) {
    state.list.push(vehicletype)
  },
  emptyList(state) {
    state.list = []
  }
}

export const getters = {
  list: state => {
    return state.list
  }
}