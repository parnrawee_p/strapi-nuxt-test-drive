export const state = () => ({
  list: []
})

export const mutations = {
  add(state, vehiclesubtype) {
    state.list.push(vehiclesubtype)
  },
  emptyList(state) {
    state.list = []
  }
}

export const getters = {
  list: state => {
    return state.list
  }
}